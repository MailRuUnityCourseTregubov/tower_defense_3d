﻿using System.Linq;
using Placement;
using UnityEditor;
using UnityEngine;

namespace Editor {
    [CustomEditor(typeof(PlacementGreed))]
    public class PlacementGreedEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            var placementGreedScript = (PlacementGreed) target;

            if (GUILayout.Button("Instantiate Object")) {
                foreach (var child in placementGreedScript.transform.Cast<Transform>().ToList()) {
                    DestroyImmediate(child.gameObject);
                }

                placementGreedScript.CreateGrid();
            }
        }
    }
}