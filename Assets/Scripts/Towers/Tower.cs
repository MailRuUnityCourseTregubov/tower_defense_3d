﻿using System.Collections.Generic;
using UnityEngine;

namespace Towers {
    public class Tower : MonoBehaviour {
        [SerializeField] private List<TowerData> levels;

        private int currentLevelIndex;
        private TowerData currentData;

        public List<TowerData> Levels => levels;

        public TowerData CurrentData {
            get => currentData;
            set {
                currentData = value;
                currentLevelIndex = levels.IndexOf(currentData);
                for (int i = 0; i < levels.Count; i++) {
                    levels[i].Visualization.SetActive(i == currentLevelIndex);
                }
            }
        }

        public TowerData GetNextLevel() {
            return levels[currentLevelIndex + 1];
        }

        public bool HasNextLevel() {
            return currentLevelIndex < levels.Count - 1;
        }

        private void Awake() {
            CurrentData = levels[0];
        }
    }
}