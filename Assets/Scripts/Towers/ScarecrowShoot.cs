﻿using UnityEngine;

namespace Towers {
    public class ScarecrowShoot : TowerShoot {
        protected override void Shoot() {
            /*
            from physics
            v*sin(a) - g*t = 0
            2*t*v*cos(a) = d
            express and substitute t
            ( v^2 sin(2a) ) / g = d
            v = sqrt( g*d / sin(2a) )
            substitute
            sin(2a) ~= 1/2
            */
            float launchForce = Mathf.Sqrt(2 * Physics.gravity.magnitude * direction.magnitude);
            var projectileInstance =
                Instantiate(tower.CurrentData.Projectile, transform.position, transform.rotation);
            projectileInstance.GetComponent<Rigidbody>().velocity = launchForce * transform.forward;
        }
    }
}