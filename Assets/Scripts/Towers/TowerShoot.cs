﻿using UnityEngine;

namespace Towers {
    public abstract class TowerShoot : MonoBehaviour {
        [SerializeField] private float rotationSpeed;
        [SerializeField] private float accuracyAngle;

        protected Tower tower;
        protected Vector3 direction;

        private Transform parentTransform;
        private float lastShotTime;

        protected void Start() {
            tower = gameObject.GetComponentInParent<Tower>();
            parentTransform = transform.parent;
        }

        private void Update() {
            Debug.DrawRay(transform.position, parentTransform.forward * 100, Color.green);
        }

        private void OnTriggerStay(Collider other) {
            if (other.gameObject.CompareTag("Enemy")) {
                direction = other.transform.position - parentTransform.position;

                parentTransform.rotation = Quaternion.Slerp(parentTransform.rotation,
                    Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);

                if (Time.time - lastShotTime > tower.CurrentData.FireRate &&
                    Vector3.Angle(parentTransform.forward, direction) < accuracyAngle) {
                    Shoot();
                    lastShotTime = Time.time;
                }
            }
        }

        protected abstract void Shoot();
    }
}