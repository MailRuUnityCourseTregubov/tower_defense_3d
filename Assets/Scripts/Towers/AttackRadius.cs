﻿using UnityEngine;

namespace Towers {
    public class AttackRadius : MonoBehaviour {
        private void OnEnable() {
            gameObject.transform.parent.GetComponentInChildren<SphereCollider>().radius =
                gameObject.GetComponentInParent<Tower>().CurrentData.AttackRadius;
        }
    }
}