﻿using System.Collections;
using Enemies;
using UnityEngine;

namespace Towers {
    public class GnomeShoot : TowerShoot {
        private ParticleSystem particle;
        private AudioSource audioSource;

        private new void Start() {
            base.Start();
            particle = gameObject.GetComponentInChildren<ParticleSystem>();
            audioSource = gameObject.GetComponentInChildren<AudioSource>();
        }

        protected override void Shoot() {
            if (Physics.Raycast(transform.position, transform.forward, out var hit, 100)) {
                if (hit.collider.CompareTag("Enemy")) {
                    StartCoroutine(ToDamage(hit.collider.GetComponent<EnemyData>()));
                    particle.Play();
                    audioSource.Play();
                }
            }
        }

        private IEnumerator ToDamage(EnemyData enemy) {
            yield return new WaitForSeconds(0.1f);
            if (enemy != null) enemy.Health -= tower.CurrentData.Damage;
        }
    }
}