﻿using Enemies;
using UnityEngine;
using UnityEngine.AI;

namespace Towers {
    public class DoghouseFreeze : MonoBehaviour {
        private bool isInsideTrigger;
        private bool isBarking;

        private AudioSource audioSource;

        private void Start() {
            audioSource = gameObject.GetComponent<AudioSource>();
            isInsideTrigger = false;
            Bark(false);
        }

        private void OnTriggerEnter(Collider other) {
            if (other.CompareTag("Enemy")) {
                int freezeSpeed = gameObject.GetComponentInParent<Tower>().CurrentData.Damage;
                if (other.GetComponent<AgentRandomSpeed>().Speed > freezeSpeed) {
                    other.GetComponent<NavMeshAgent>().speed = freezeSpeed;
                    other.GetComponentInChildren<Animator>().SetFloat(Animator.StringToHash("Speed"), freezeSpeed);
                }

                if (!isBarking) {
                    Bark(true);
                }
            }
        }

        private void OnTriggerExit(Collider other) {
            if (other.CompareTag("Enemy")) {
                float defaultSpeed = other.GetComponent<AgentRandomSpeed>().Speed;
                other.GetComponent<NavMeshAgent>().speed = defaultSpeed;
                other.GetComponentInChildren<Animator>().SetFloat(Animator.StringToHash("Speed"), defaultSpeed);
            }
        }

        private void OnTriggerStay(Collider other) {
            if (other.CompareTag("Enemy")) {
                isInsideTrigger = true;
            }
        }

        private void FixedUpdate() {
            isInsideTrigger = false;
        }

        private void LateUpdate() {
            if (!isInsideTrigger && isBarking) {
                Bark(false);
            }
        }

        private void Bark(bool value) {
            isBarking = value;
            transform.parent.GetComponentInChildren<Animator>().enabled = value;
            if (value) {
                audioSource.Play();
            }
            else {
                audioSource.Stop();
            }
        }
    }
}