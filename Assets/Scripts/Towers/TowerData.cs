﻿using System;
using UnityEngine;

namespace Towers {
    [Serializable]
    public class TowerData {
        [SerializeField] private int cost;
        [SerializeField] private int sale;
        [SerializeField] private GameObject visualization;
        [SerializeField] private GameObject projectile;
        [SerializeField] private float fireRate;
        [SerializeField] private float attackRadius;
        [SerializeField] private int damage;

        public int Cost => cost;
        public int Sale => sale;
        public GameObject Visualization => visualization;
        public GameObject Projectile => projectile;
        public float FireRate => fireRate;
        public float AttackRadius => attackRadius;
        public int Damage => damage;
    }
}