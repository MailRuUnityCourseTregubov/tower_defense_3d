﻿using System.Collections.Generic;
using Enemies;
using UnityEngine;

public class HencoopController : MonoBehaviour {
    [SerializeField] private GameObject chickenBar;
    [SerializeField] private GameObject chickenImagePrefab;
    [SerializeField] private int chickens;

    private GameManager gameManager;
    private Stack<GameObject> chickensImages;
    private Vector3 shift;
    private AudioSource audioSource;

    private void Start() {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        chickensImages = new Stack<GameObject>();
        shift = new Vector3(chickenImagePrefab.GetComponent<RectTransform>().rect.width, 0, 0);
        GenerateChickenImages(chickens);
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Enemy")) {
            Destroy(other.gameObject);
            for (int i = 0; i < other.GetComponent<EnemyData>().Damage; i++) {
                if (chickens > 0) {
                    --chickens;
                    Destroy(chickensImages.Pop());
                    audioSource.Play();
                }

                if (chickens <= 0) {
                    gameManager.GameOver(false);
                }
            }
        }
    }

    private void GenerateChickenImages(int count) {
        foreach (Transform child in chickenBar.transform) {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < count / 2; i++) {
            GenerateChickenImage(i);
            GenerateChickenImage(i);
        }

        if (count % 2 == 1) {
            GenerateChickenImage(count / 2);
        }
    }

    private void GenerateChickenImage(int position) {
        var tmp = Instantiate(chickenImagePrefab, chickenBar.transform);
        tmp.transform.position -= shift * position;
        chickensImages.Push(tmp);
    }
}