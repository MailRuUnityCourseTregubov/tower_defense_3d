﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour {
    private Animator animator;
    private string sceneName;

    private void Start() {
        animator = gameObject.GetComponent<Animator>();
    }

    public void LoadScene(string sceneName) {
        this.sceneName = sceneName;
        animator.SetTrigger(Animator.StringToHash("FadeOut"));
    }

    public void OnFadeComplete() {
        SceneManager.LoadScene(sceneName);
    }
}