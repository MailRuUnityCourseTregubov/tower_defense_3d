﻿using Enemies;
using UnityEngine;

namespace Projectiles {
    public class PumpkinAffectorController : MonoBehaviour {
        [SerializeField] private GameObject explosion;
        [SerializeField] private int damage;

        private void OnTriggerEnter(Collider other) {
            if (other.gameObject.CompareTag("Enemy")) {
                other.GetComponent<EnemyData>().Health -= damage;
            }

            Destroy(transform.parent.gameObject);
        }

        private void OnEnable() {
            Instantiate(explosion, transform.position, transform.rotation);
        }
    }
}