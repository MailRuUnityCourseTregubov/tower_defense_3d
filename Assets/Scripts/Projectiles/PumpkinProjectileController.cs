﻿using UnityEngine;

namespace Projectiles {
    public class PumpkinProjectileController : MonoBehaviour {
        private void OnTriggerEnter(Collider other) {
            if (!other.CompareTag("Player")) {
                foreach (Transform child in transform) {
                    if (child.CompareTag("Affector")) {
                        child.gameObject.SetActive(true);
                    }
                }
            }
        }
    }
}