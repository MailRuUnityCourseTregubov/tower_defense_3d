﻿using UnityEngine;

namespace Placement {
    public class PlacementGreed : MonoBehaviour {
        [SerializeField] private GameObject placementTilePrefab;
        [SerializeField] private Vector2 gridSize;
        [SerializeField] private int placementTileSize;

        public void CreateGrid() {
            placementTilePrefab.transform.localScale = new Vector3(placementTileSize, 1, placementTileSize);
            var spawnPosition = gameObject.transform.position;
            for (int i = 0; i < gridSize.x; i++) {
                for (int j = 0; j < gridSize.y; j++) {
                    var placementTile = Instantiate(placementTilePrefab,
                        spawnPosition + new Vector3(i * placementTileSize, 0, j * placementTileSize), Quaternion.identity);
                    placementTile.transform.parent = gameObject.transform;
                }
            }
        }
    }
}