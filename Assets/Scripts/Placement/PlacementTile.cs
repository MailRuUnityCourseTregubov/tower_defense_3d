﻿using UnityEngine;

namespace Placement {
    public class PlacementTile : MonoBehaviour {
        [SerializeField] private Material emptyMaterial;
        [SerializeField] private Material filledMaterial;

        private bool isEmpty;
        private Renderer tileRenderer;

        public bool IsEmpty {
            get => isEmpty;
            set {
                isEmpty = value;
                SetStateVisualization(!value);
            }
        }

        private void Start() {
            isEmpty = true;
            tileRenderer = gameObject.GetComponentInChildren<Renderer>();
        }

        private void OnMouseEnter() {
            if (isEmpty) {
                SetStateVisualization(true);
            }
        }

        private void OnMouseExit() {
            if (isEmpty) {
                SetStateVisualization(false);
            }
        }

        private void SetStateVisualization(bool isFilled) {
            tileRenderer.sharedMaterial = isFilled ? filledMaterial : emptyMaterial;
        }
    }
}