﻿using System.Collections.Generic;
using System.Linq;
using Towers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Placement {
    public class TowerPlacement : MonoBehaviour {
        [SerializeField] private Toggle createToggle;
        [SerializeField] private ToggleGroup toggleGroup;
        [SerializeField] private GameObject towersButtons;
        [SerializeField] private GameObject towersGhost;
        [SerializeField] private List<GameObject> towersPrefabs;

        private Tower selectedTower;
        private PlacementTile selectedTile;

        private Camera mainCamera;
        private GameManager gameManager;
        private List<Toggle> toggles;
        private AudioSource audioSource;

        private void Start() {
            if (Camera.main != null) mainCamera = Camera.main;
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            toggles = new List<Toggle>(toggleGroup.GetComponentsInChildren<Toggle>());
            audioSource = gameObject.GetComponent<AudioSource>();
        }

        private void Update() {
            if (createToggle.isOn) {
                DeselectTower();
                if (toggleGroup.AnyTogglesOn()) {
                    MoveTowerToMouse(towersGhost);

                    if (Input.GetMouseButtonDown(0)) {
                        InstantiateTower(towersPrefabs[toggles.IndexOf(toggleGroup.ActiveToggles().FirstOrDefault())]);
                    }

                    if (Input.GetMouseButtonDown(1)) {
                        toggleGroup.SetAllTogglesOff();
                    }
                }
                else if (Input.GetMouseButtonDown(0)) {
                    SelectTower();
                }
                else if (Input.GetMouseButtonDown(1)) {
                    createToggle.isOn = false;
                }
            }
            else if (towersButtons.activeSelf && Input.GetMouseButtonDown(1)) {
                DeselectTower();
            }
        }

        public void UpgradeTower() {
            if (selectedTower != null) {
                var tower = selectedTower;
                if (CanUpgradeTower(tower)) {
                    tower.CurrentData = tower.GetNextLevel();
                    gameManager.PlayerGold -= tower.CurrentData.Cost;
                    audioSource.Play();
                    DeselectTower();
                }
            }
        }

        public void SaleTower() {
            if (selectedTower != null && selectedTile != null) {
                gameManager.PlayerGold += selectedTower.CurrentData.Sale;
                audioSource.Play();
                Destroy(selectedTower.gameObject);
                selectedTile.IsEmpty = true;
                DeselectTower();
            }
        }

        private void InstantiateTower(GameObject towerPrefab) {
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit) &&
                hit.collider.CompareTag("Placement")) {
                var placementTile = hit.collider.GetComponent<PlacementTile>();

                if (CanPlaceTower(placementTile, towerPrefab)) {
                    var t = Instantiate(towerPrefab, hit.collider.transform.position, Quaternion.identity);
                    gameManager.PlayerGold -= t.GetComponent<Tower>().CurrentData.Cost;
                    audioSource.Play();
                    placementTile.IsEmpty = false;

                    toggleGroup.SetAllTogglesOff();
                    createToggle.isOn = false;
                }
            }
        }

        private void SelectTower() {
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit) &&
                hit.collider.CompareTag("Player") && !EventSystem.current.IsPointerOverGameObject()) {
                towersButtons.SetActive(true);
                towersButtons.transform.position = Input.mousePosition;

                selectedTower = hit.collider.GetComponent<Tower>();
                if (Physics.Raycast(selectedTower.gameObject.transform.position + new Vector3(0, 1, 0),
                        Vector3.down, out var towerHit) && towerHit.collider.CompareTag("Placement")) {
                    selectedTile = towerHit.collider.GetComponent<PlacementTile>();
                }

                toggleGroup.SetAllTogglesOff();
                createToggle.isOn = false;
            }
        }

        private void DeselectTower() {
            towersButtons.SetActive(false);
            selectedTower = null;
            selectedTile = null;
        }

        private void MoveTowerToMouse(GameObject towerGhost) {
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit)) {
                towerGhost.transform.position = hit.point;
            }
        }

        private bool CanPlaceTower(PlacementTile placementTile, GameObject towerPrefab) {
            return placementTile.IsEmpty && gameManager.PlayerGold >= towerPrefab.GetComponent<Tower>().Levels[0].Cost;
        }

        private bool CanUpgradeTower(Tower tower) {
            return tower.HasNextLevel() && gameManager.PlayerGold >= tower.GetNextLevel().Cost;
        }
    }
}