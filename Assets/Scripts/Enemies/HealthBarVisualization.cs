﻿using UnityEngine;

namespace Enemies {
    public class HealthBarVisualization : MonoBehaviour {
        private Transform cameraToFace;
        private EnemyData enemyData;
        private GameObject bar;

        private float maxHealth;
        private float originalScale;
        private Vector3 tmpScale;

        private void Start() {
            if (Camera.main != null) cameraToFace = Camera.main.transform;

            enemyData = gameObject.GetComponentInParent<EnemyData>();
            maxHealth = enemyData.Health;

            bar = transform.GetChild(0).GetChild(0).gameObject;
            originalScale = bar.transform.localScale.x;
        }

        private void Update() {
            transform.forward = -cameraToFace.transform.forward;

            tmpScale = bar.transform.localScale;
            tmpScale.x = enemyData.Health / maxHealth * originalScale;
            bar.transform.localScale = tmpScale;
        }
    }
}