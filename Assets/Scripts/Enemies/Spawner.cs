﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemies {
    public class Spawner : MonoBehaviour {
        [SerializeField] private List<GameObject> enemies;
        [SerializeField] private List<GameObject> bosses;
        [SerializeField] private int enemiesInWave;
        [SerializeField] private int wavesPerBoss;
        [SerializeField] private float startWait;
        [SerializeField] private float spawnWait;
        [SerializeField] private float waveWait;
        [SerializeField] private float bossWait;
        [SerializeField] private int wavesCycles;

        private Vector3 spawnPosition;
        private int randomObjectIndex;

        private GameManager gameManager;

        private void Start() {
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            spawnPosition = transform.position;
            StartCoroutine(SpawnWaves());
        }

        private IEnumerator SpawnWaves() {
            yield return new WaitForSeconds(startWait);
            for (int k = 0; k < wavesCycles; k++) {
                for (int i = 0; i < wavesPerBoss; i++) {
                    randomObjectIndex = Random.Range(0, enemies.Count);
                    for (var j = 0; j < enemiesInWave; j++) {
                        Instantiate(enemies[randomObjectIndex], spawnPosition, Quaternion.identity);
                        yield return new WaitForSeconds(spawnWait);
                    }

                    yield return new WaitForSeconds(waveWait);
                }

                randomObjectIndex = Random.Range(0, bosses.Count);
                Instantiate(bosses[randomObjectIndex], spawnPosition, Quaternion.identity);
                yield return new WaitForSeconds(bossWait);
            }

            StartCoroutine(Win());
        }

        private IEnumerator Win() {
            while (true) {
                if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0) {
                    gameManager.GameOver(true);
                }

                yield return new WaitForSeconds(2);
            }
        }
    }
}