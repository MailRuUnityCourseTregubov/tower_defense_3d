﻿using UnityEngine;

namespace Enemies {
    public class EnemyData : MonoBehaviour {
        [SerializeField] private int damage;
        [SerializeField] private int cost;
        [SerializeField] private int health;

        private GameManager gameManager;

        public int Damage => damage;

        public int Health {
            get => health;
            set {
                health = value;
                if (health <= 0) {
                    gameManager.PlayerGold += cost;
                    Destroy(gameObject);
                }
            }
        }

        private void Start() {
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }
    }
}