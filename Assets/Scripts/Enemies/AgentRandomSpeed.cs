﻿using UnityEngine;
using UnityEngine.AI;

namespace Enemies {
    public class AgentRandomSpeed : MonoBehaviour {
        [SerializeField] private Vector2 range;

        private float speed;

        public float Speed => speed;

        private void Awake() {
            speed = Random.Range(range.x, range.y);
            gameObject.GetComponent<NavMeshAgent>().speed = speed;
            gameObject.GetComponentInChildren<Animator>().SetFloat(Animator.StringToHash("Speed"), speed);
        }
    }
}