﻿using UnityEngine;
using UnityEngine.AI;

namespace Enemies {
    public class AgentMoveTo : MonoBehaviour {
        [SerializeField] private Transform goal;

        private void Start() {
            var agent = gameObject.GetComponent<NavMeshAgent>();
            agent.destination = goal.position;
        }
    }
}