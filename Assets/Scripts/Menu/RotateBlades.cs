﻿using UnityEngine;

namespace Menu {
    public class RotateBlades : MonoBehaviour {
        [SerializeField] private float angularSpeed;

        private Camera mainCamera;
        private Vector3 prevHitPoint;

        private void Start() {
            if (Camera.main != null) mainCamera = Camera.main;
            prevHitPoint = Vector3.zero;
        }

        private void Update() {
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit)) {
                if (Input.GetMouseButton(0)) {
                    RotateAroundPivot(Vector3.Dot(hit.point - transform.position - prevHitPoint, hit.point));
                }
                else {
                    RotateAroundPivot(angularSpeed * Time.deltaTime);
                }

                prevHitPoint = hit.point - transform.position;
            }
            else {
                RotateAroundPivot(angularSpeed * Time.deltaTime);
            }
        }

        private void RotateAroundPivot(float angle) {
            transform.RotateAround(transform.position, transform.forward, -angle);
        }
    }
}