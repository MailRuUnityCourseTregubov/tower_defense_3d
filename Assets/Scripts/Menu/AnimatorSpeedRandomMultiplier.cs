﻿using System.Collections;
using UnityEngine;

namespace Menu {
    public class AnimatorSpeedRandomMultiplier : MonoBehaviour {
        [SerializeField] private Vector2 range;
        [SerializeField] private float changeAfterSeconds;

        private Animator animator;
        private static readonly int AnimationSpeed = Animator.StringToHash("AnimationSpeedMultiplier");

        private void Start() {
            animator = gameObject.GetComponent<Animator>();
            StartCoroutine(ChangeAnimatorSpeed());
        }

        private IEnumerator ChangeAnimatorSpeed() {
            while (true) {
                animator.SetFloat(AnimationSpeed, Random.Range(range.x, range.y));
                yield return new WaitForSeconds(changeAfterSeconds);
            }
        }
    }
}