﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Menu {
    public class ChickenBehavior : MonoBehaviour {
        [SerializeField] private Vector2 peckingTimeRange;
        [SerializeField] private float moveRadius;

        private NavMeshAgent agent;
        private bool waiting;

        private Animator animator;
        private static readonly int Pecking = Animator.StringToHash("Pecking");
        private static readonly int Walking = Animator.StringToHash("Walking");

        private void Start() {
            agent = GetComponent<NavMeshAgent>();
            agent.autoBraking = false;

            animator = gameObject.GetComponentInChildren<Animator>();
        }

        private void Update() {
            if (!agent.pathPending && agent.remainingDistance < 0.5f && !waiting)
                StartCoroutine(GoToNextPoint());
        }

        private IEnumerator GoToNextPoint() {
            waiting = true;
            animator.SetTrigger(Pecking);

            yield return new WaitForSeconds(Random.Range(peckingTimeRange.x, peckingTimeRange.y));

            animator.SetTrigger(Walking);
            waiting = false;

            agent.destination = RandomNavMeshLocation(moveRadius);
        }

        private Vector3 RandomNavMeshLocation(float radius) {
            var randomDirection = Random.insideUnitSphere * radius + transform.position;
            var finalPosition = Vector3.zero;
            if (NavMesh.SamplePosition(randomDirection, out var hit, radius, 1)) {
                finalPosition = hit.position;
            }

            return finalPosition;
        }
    }
}