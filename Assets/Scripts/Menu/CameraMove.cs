﻿using UnityEngine;

namespace Menu {
    public class CameraMove : MonoBehaviour {
        [SerializeField] private float movementSpeed;
        [SerializeField] private float rotationSpeed;

        private AudioSource whoosh;

        private Transform point;

        public Transform Point {
            get => point;
            set {
                point = value;
                whoosh.Play();
            }
        }

        private void Start() {
            point = gameObject.transform;
            whoosh = gameObject.GetComponent<AudioSource>();
        }

        private void LateUpdate() {
            transform.position = Vector3.Slerp(transform.position, point.position, Time.deltaTime * movementSpeed);
            transform.rotation = Quaternion.Slerp(transform.rotation, point.rotation, Time.deltaTime * rotationSpeed);
        }
    }
}