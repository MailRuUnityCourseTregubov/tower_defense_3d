﻿using UnityEngine;

namespace Menu {
    public class MainMenu : MonoBehaviour {
        [SerializeField] private Transform homePoint;
        [SerializeField] private Transform selectLevelPoint;

        [SerializeField] private GameObject levelButtons;
        [SerializeField] private SceneTransition sceneTransition;

        private CameraMove mainCamera;

        private void Start() {
            if (Camera.main != null) mainCamera = Camera.main.GetComponent<CameraMove>();
            levelButtons.SetActive(false);
        }

        private void Update() {
            if (Input.GetMouseButtonDown(1)) {
                Home();
            }
        }

        public void SelectLevel() {
            levelButtons.SetActive(true);
            mainCamera.Point = selectLevelPoint;
        }

        public void LoadLevel(string level) {
            sceneTransition.LoadScene(level);
        }

        public void Home() {
            levelButtons.SetActive(false);
            mainCamera.Point = homePoint;
        }

        public void Exit() {
            Application.Quit();
            Debug.Log("Exit");
        }
    }
}