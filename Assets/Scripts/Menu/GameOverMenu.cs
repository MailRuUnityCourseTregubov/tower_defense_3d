﻿using UnityEngine;
using UnityEngine.UI;

namespace Menu {
    public class GameOverMenu : MonoBehaviour {
        [SerializeField] private Text whoWonLabel;
        [SerializeField] private Text nextSceneButtonText;

        [SerializeField] private GameObject foxes;
        [SerializeField] private GameObject chickens;

        [SerializeField] private SceneTransition sceneTransition;
        [SerializeField] private string homeScene;
        [SerializeField] private string defaultLevel;

        private bool isWon;
        private string nextScene;

        private void Start() {
            nextScene = PlayerPrefs.HasKey("NextScene") ? PlayerPrefs.GetString("NextScene") : defaultLevel;
            isWon = PlayerPrefs.HasKey("IsWon") && PlayerPrefs.GetInt("IsWon") != 0;

            whoWonLabel.text = isWon ? "CHICKENS SAFE" : "FOXES WIN";
            nextSceneButtonText.text = isWon ? "NEXT LVL" : "RESTART";

            foxes.SetActive(!isWon);
            chickens.SetActive(isWon);
        }

        public void Home() {
            sceneTransition.LoadScene(homeScene);
        }

        public void NextScene() {
            sceneTransition.LoadScene(nextScene);
        }
    }
}