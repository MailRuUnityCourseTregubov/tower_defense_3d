﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    [SerializeField] private int playerGold;
    [SerializeField] private Text goldLabel;

    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private SceneTransition sceneTransition;

    [SerializeField] private string homeScene;
    [SerializeField] private string nextLevel;
    [SerializeField] private string gameOverScene;

    private Camera mainCamera;
    private bool gameOver;

    public int PlayerGold {
        get => playerGold;
        set {
            playerGold = value;
            goldLabel.text = playerGold.ToString();
        }
    }

    private void Start() {
        if (Camera.main != null) mainCamera = Camera.main;
        goldLabel.text = playerGold.ToString();
        FreezeGame(false);
        gameOver = false;
    }

    public void Pause(bool value) {
        FreezeGame(value);
    }

    public void Exit() {
        FreezeGame(false);
        sceneTransition.LoadScene(homeScene);
    }

    public void Restart() {
        FreezeGame(false);
        sceneTransition.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void NextLevel() {
        FreezeGame(false);
        sceneTransition.LoadScene(nextLevel);
    }

    public void GameOver(bool win) {
        if (!gameOver) {
            FreezeGame(false);
            gameOver = true;

            PlayerPrefs.SetInt("IsWon", win ? 1 : 0);
            PlayerPrefs.SetString("NextScene", win ? nextLevel : SceneManager.GetActiveScene().name);
            sceneTransition.LoadScene(gameOverScene);
        }
    }

    private void FreezeGame(bool freeze) {
        Time.timeScale = freeze ? 0 : 1;
        audioMixer.SetFloat("FXVolume", freeze ? -80 : 0);
        mainCamera.eventMask = freeze ? LayerMask.GetMask("UI") : -1;
    }
}